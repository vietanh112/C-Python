#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <functional>

using hr_clock = std::chrono::high_resolution_clock;
using std::chrono::microseconds;
using std::chrono::duration_cast;


// https://www.tutorialcup.com/leetcode-solutions/edit-distance-leetcode-solution.htm
class Solution {
public:
    int minDistance3(std::string word1, std::string word2) {
        int m = word1.size(), n = word2.size(), pre;
        std::vector<int> cur(n + 1, 0);
        for (int j = 1; j <= n; j++) {
            cur[j] = j;
        }
        for (int i = 1; i <= m; i++) {
            pre = cur[0];
            cur[0] = i;
            for (int j = 1; j <= n; j++) {
                int temp = cur[j];
                if (word1[i - 1] == word2[j - 1]) {
                    cur[j] = pre;
                } 
                else {
                    cur[j] = std::min(pre, std::min(cur[j - 1], cur[j])) + 1;
                }
                pre = temp;
            }
        }
        return cur[n];
    }


    int minDistance2(std::string word1, std::string word2) {
        int m = word1.size(), n = word2.size();
        std::vector<int> pre(n + 1, 0), cur(n + 1, 0);
        for (int j = 1; j <= n; j++) {
            pre[j] = j;
        }
        for (int i = 1; i <= m; i++) {
            cur[0] = i;
            for (int j = 1; j <= n; j++) {
                if (word1[i - 1] == word2[j - 1]) {
                    cur[j] = pre[j - 1];
                } else {
                    cur[j] = std::min(pre[j - 1], std::min(cur[j - 1], pre[j])) + 1;
                }
            }
            fill(pre.begin(), pre.end(), 0);
            swap(pre, cur);
        }
        return pre[n];
    }


    int minDistance1(std::string word1, std::string word2) {
        int m = word1.size(), n = word2.size();
        std::vector<std::vector<int>> dp(m + 1, std::vector<int>(n + 1, 0));
        for (int i = 1; i <= m; i++) {
            dp[i][0] = i;
        }
        for (int j = 1; j <= n; j++) {
            dp[0][j] = j;
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (word1[i - 1] == word2[j - 1]) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = std::min(dp[i - 1][j - 1], std::min(dp[i][j - 1], dp[i - 1][j])) + 1;
                }
            }
        }
        return dp[m][n];
    }


    int min_edit_distance(std::string word1, std::string word2) {
        /**
         * http://www.giaithuatlaptrinh.com/?tag=edit-distance&paged=2
         */
        size_t m = word1.size();
        size_t n = word2.size();
        int ed[m+1][n+1];
        int i, j;

        for (i = 0; i <= m; ++i) ed[i][0] = i;
        for (j = 1; j <= n; ++j) ed[0][j] = j;

        int del, ins, edit;
        for (i = 1; i <=m; ++i){
            for (j = 1; j <=n; ++j){
                del = ed[i-1][j] + 1;
                ins = ed[i][j-1] + 1;
                edit = ed[i-1][j-1];
                if (i == 1 && j==1 ) std::cout << del << ' ' << ins << ' ' << edit <<'\n';
                if (word1[i] != word2[j]) ++edit;
                if (i == 1 && j==1 ) std::cout << edit << '\n';
                ed[i][j] = std::min({del, ins, edit});
                // std::cout << "[" << i << "][" << j << "] = " << ed[i][j] << "   ";
            }
            // std::cout << '\n';
        }


        for (i = 0; i <= m; ++i){
            for (j = 0; j <= n; ++j){
                std::cout << ed[i][j] << ' ';
            }
            std::cout << std::endl;
        }

        return ed[m][n];
    }
};

Solution solution;

void action (std::string src, std::string dst, 
             const unsigned int del, 
             const unsigned int ins, 
             const unsigned int edit
) {
    
}

microseconds measure_runtime(
    std::string word1, std::string word2, 
    // int (*func) (std::string, std::string)
    std::function<int(std::string, std::string)> func
) {
    hr_clock::time_point start, end;
    microseconds duration;
    int distance;

    start = hr_clock::now();
    // distance = func(word1, word2);
    func(word1, word2);
    end = hr_clock::now();
    
    duration = duration_cast<microseconds>(end - start);
    std::cout  << __PRETTY_FUNCTION__ << " -->  "<< duration.count() << '\n';

    return duration;
}
// Solution solution;

int minDistance1(std::string word1, std::string word2) 
    { return solution.minDistance1(word1, word2); }

int minDistance2(std::string word1, std::string word2)
    { return solution.minDistance2(word1, word2); }

int minDistance3(std::string word1, std::string word2)
    { return solution.minDistance3(word1, word2); }


int main(){
    // Solution solution;
    std::string word1 = "1234567828dwadwadwa4200000000000000000000000000000000000000000000";
    std::string word2 = "12345678289626264894415555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555";
    microseconds runtime;

    runtime = measure_runtime(word1, word2, &minDistance1);
    runtime = measure_runtime(word1, word2, &minDistance2);
    runtime = measure_runtime(word1, word2, &minDistance3);

    return 0;
}


int main1(){
    std::string word1 = "horse";
    std::string word2 = "ros";
    int ed = solution.min_edit_distance(word1, word2);
    std::cout << "ed = "<< ed << '\n';
    return 0;
<<<<<<< HEAD
}
=======
}
>>>>>>> 17ab59cb16e33474dfd928e98d630a9d6cb142e0
