#include <bits/stdc++.h>


class Solution {
public:
    int getMinDistance(std::vector<int>& nums, int target, int start) {
        int minAbs = std::numeric_limits<int>::max();
        int i, absolute, size =  nums.size();
        for (i = 0; i < size; ++i) {
            if (nums[i] == target){
                absolute = abs(i-start);
                if (absolute <= minAbs){
                    minAbs = absolute;
                }
            }
        }
        return minAbs;
    }
};