#include <string>
#include <iostream>
#include <cstring>
#include <algorithm>


using namespace std;

int arr[] = { 0, 1, 0, 1, 0 };

class Solution {
public:
    string longestPalindrome2(string s) {
        if (s.size()<=1) 
            return s;
        int min_left=0;
        int max_len=1;
        int max_right=s.size()-1;
        for (int mid=0;mid<s.size();){
            int left=mid;
            int right=mid;
            while ((right<max_right) && (s[right+1]==s[right])){
                right++;
            } // Skip duplicate characters in the middle

            mid=right+1; //for next iter
            while (right<max_right && left>0 && s[right+1]==s[left-1]){
                right++; 
                left--;
            } // Expand the selection as long it is a palindrom

            int new_len=right-left+1; //record best palindro
            if (new_len>max_len){ 
                min_left=left; 
                max_len=new_len; 
            }
        }
        return s.substr(min_left, max_len);
    }


    char* longestPalindrome3(char* s) {
        if (strlen(s) <= 1) return s;

        char *go,*max_start,*left,*right,*re;
        int max_length,temp_length;
        char *s_end;
        s_end = s+strlen(s);


        for(go = s,max_length = 1;go<s_end;){
            if (s_end - go <= max_length * sizeof(char) / 2) 
                break;
            left = go, right = go;

            while(*right==*(right+1)) 
                right++;

            go = right + 1;

            while(left<s_end && left>s && *(left-1) == *(right+1)){
                right++;
                left--;
            }
            temp_length = (right-left)/sizeof(char) + 1;

            if(temp_length > max_length) {
                max_start = left;
                max_length = temp_length;
            }
        }

        re =(char *) malloc(max_length*sizeof(char)+1);
        go = re;
        while(max_length){
            *(go) = *(max_start);
            go++;
            max_start++;
            max_length--;
        }
        *go = '\0';

        return re;
    }


    string longestPalindrome(string s) {
        unsigned int size = s.length();
        if (size <= 1) 
            return s;

        unsigned int length, maxlen = 1;
        unsigned int left, right, start = 0, maxRight = size-1;
        for (int mid = 0; mid < size; ++ mid) {
            left = mid;
            right = mid;

            while ((right < size) && (s[mid] == s[right+1])) {
                ++right;
            } // Skip duplicate characters in the middle
            
            while ((left > 0) && (right < maxRight) && (s[left-1] == s[right+1]))
            {
                -- left;
                ++ right;
            } // expande and check from middle

            length = ++right - left;
            if (length >= maxlen) {
                maxlen = length;
                start = left;
            }
        }
        return s.substr(start, maxlen);
    }
};


int main()
{
    Solution solution;
    std::string sub = solution.longestPalindrome("baabad");
    std::cout << sub << '\n';
    return 0;
}