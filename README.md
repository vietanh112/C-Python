## link ref
 * https://www.facebook.com/code.mely/photos/pcb.131050736246911/131048719580446
 * https://www.geeksforgeeks.org/how-to-call-c-c-from-python/
 * https://code.visualstudio.com/docs/cpp/config-linux
 * https://blog.luyencode.net/cach-tach-code-c-thanh-file-h-va-cpp/#tai-sao-phai-tach-file

## library
1. [Threading Building Blocks](https://github.com/oneapi-src/oneTBB)
2. [openMP](https://en.wikipedia.org/wiki/OpenMP)
3. [High Performance ParalleX](https://github.com/STEllAR-GROUP/hpx)
4. openSSL
5. [pybind11](https://github.com/pybind/pybind11)
6. [boost](https://www.boost.org/) - [install](https://gitlab.com/vietanh112/big-number/-/blob/main/3rd_party/install_boost_1_81_0.sh) in ubuntu22.04 
7. opencv