#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

std::vector<int> vec;
std::mutex m;

void push(){
	m.lock();
	for (int i = 0; i != 10; ++i){
		std::cout << i << " --> |\n";

		// sleep(1);
		vec.push_back(i);
	}
	m.unlock();
}

void pop(){
	m.lock();
	for (int i = 0; i != 10; ++i){
		if (vec.size() > 0){
			int val = vec.back();
			vec.pop_back();
			std::cout << "      | --> "<< val <<'\n';
		}
	// sleep(1);
	m.unlock();
	}
}

void concurrent_access(){
	/**
	 * mỗi lần chạy sẽ cho kết quả khác nhau
	 * do các thread k đồng bộ
	 */ 
	std::thread pushT(push);
	std::thread popT(pop);

	if (pushT.joinable())
		pushT.join();
	if (popT.joinable())
		popT.join();
}


int main(int argc, char* argv[]){
	concurrent_access();
}