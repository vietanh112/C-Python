#include <iostream>
#include <thread>
// #include <algorithm>
// #include <vector>
// #include <map>
#include <cstring>

void threadFunc(const char* msg){
    // sleep(2);
    if (std::strlen(msg) == 0){
        // get id using namespace this_thread
        std::thread::id id2 = std::this_thread::get_id(); 
        std::cout << "std::this_thread::get_id() " << id2 << "\n";
    }
    else
	    std::cout << msg << std::endl;
}

void init_thread_with_func(){
    std::thread t1(&threadFunc, "");
    std::thread::id id1 = t1.get_id();                  // get id of thread
    // std::thread::id id2 = std::this_thread::get_id();   // get id using namespace this_thread


    // funcTest1.detach();
    if (t1.joinable()) {
        t1.join();

        // 2 cách để get thread_id
        std::cout << "std::thread::id::get_id()  " << id1 << " is terminated\n";
    }
    else {
        std::cout <<  "functTest1 is detached" << '\n';
    }
    // sleep(3);
}


class MyFunctor{
public:
	void operator()(int* arr, int length){
    // void operator()(){   // trường hợp không tham số  func này giống __call__ trong python
		std::cout << "An array of length " << length << " is passed to thread\n";
		for (int i = 0; i != length; ++i)
			std::cout << arr[i] << " ";
		std::cout << '\n';
	}

    void publicFunction(int* arr, int length){
        std::cout << "An arrray of length " << length << " is passed to thread\n";
        for (int i = 0; i != length; ++i)
            std::cout << arr[i] << " ";
        std::cout << "\n\nChanging sign of all elements of initial array\n";


        for (int i = 0; i != length; ++i){
            arr[i] *= -1;
            std::cout << arr[i] << " ";
        }
        std::cout << '\n';
    }
};

void init_thread_with_object(){
    MyFunctor func;
    int arr[5] = { 1, 3, 5, 7, 9 };

    std::thread threadTest(func, arr, 5);
    if (threadTest.joinable()){
        threadTest.join();
    }
}

void init_thread_with_public_func_of_class(){
    MyFunctor fn2;
    int arr2[5] = { -1, 3, 5, -7, 0 };

    std::thread threadTest2(&MyFunctor::publicFunction, &fn2, arr2, 5);


    if (threadTest2.joinable()){
        threadTest2.join();
    }
}

/**
 * cách tạo thread từ func, operator of class, and public member of class
 * 2 dạng có tham số và k có tham số truyền vào.
 * ref: https://viblo.asia/p/lam-quen-voi-multithreading-trong-c-qm6RWQYXGeJE
 */

int main(int argv, char* argc[]){
    /** 
     * NOTE add flag -pthread to compile if #include <thread>
     * https://stackoverflow.com/questions/34933042/undefined-reference-to-pthread-create-error-when-making-c11-application-with
     * g++-11 -v -std=c++20 -pthread stl/multithreading.cpp -o test_thread && echo "\n" && ./test_thread
    */
    
    init_thread_with_func();
    // init_thread_with_object();
    // init_thread_with_public_func_of_class();

    return 0;
}