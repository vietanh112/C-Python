#include <iostream>
#include <chrono>
#include <cmath>


int main(int argc, char** argv){
    // https://viblo.asia/p/gioi-thieu-openmp-trong-c-phan-1-GrLZDAEBlk0
    const int size = 1024;
    double sinTable[size];
    
    std::chrono::high_resolution_clock::time_point 
        start = std::chrono::high_resolution_clock::now();

    #pragma omp parallel for
    for(int n=0; n<size; ++n){
        sinTable[n] = std::sin(2 * M_PI * n / size);
    }

    std::chrono::high_resolution_clock::time_point 
        stop = std::chrono::high_resolution_clock::now();

    std::chrono::microseconds 
        duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        
    std::cout << duration.count() << '\n';

    return 0;
}
