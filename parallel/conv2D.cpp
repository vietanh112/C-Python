#include <iostream>
#include <thread>
#include <chrono>
#include "conv2D.h"


Conv2D::Conv2D(
    std ::uint32_t  in_channels,
    std ::uint32_t  out_channels,
    std ::uint8_t   kernel_size,
    std ::uint8_t   stride,
    std ::uint8_t   padding,
    const char*     padding_mode,
    bool            bias
) {
    this->in_channels  = in_channels ;
    this->out_channels = out_channels;
    this->kernel_size  = kernel_size ;
    this->stride       = stride      ;
    this->padding      = padding     ;
    this->padding_mode = padding_mode;
    this->bias         = bias        ;
}

void Conv2D::forward(){

}

void Conv2D::backward(){

}

