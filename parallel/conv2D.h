#pragma once
#include <cstdint>

struct Kernel2D
{
	float x;
	float y;
};
class Tensor{
public:
    std::uint8_t nd = 3;
    Tensor(std::uint8_t nd = 4){
        this->nd = nd;
    }
};

class Conv2D{
public:
    std::uint32_t in_channels, out_channels;
    std::uint8_t kernel_size = 3, stride = 1, padding = 0;
    // std::uint8_t dilation;
    const char* padding_mode = "zeros"; // reflect // replicate // circular
    bool bias = true;

    Conv2D(
        std ::uint32_t  in_channels,
        std ::uint32_t  out_channels,
        std ::uint8_t   kernel_size,
        std ::uint8_t   stride,
        std ::uint8_t   padding,
        const char*     padding_mode,
        bool            bias
    );
    ~Conv2D();
    
    void forward();
    void backward();
};