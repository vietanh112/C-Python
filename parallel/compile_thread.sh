# https://stackoverflow.com/questions/34933042/undefined-reference-to-pthread-create-error-when-making-c11-application-with
clear
# g++-11 -std=c++20 -pthread multithreading/multithreading.cpp -o test_thread && echo "\n" && ./test_thread
g++-11 -std=c++20 -fopenmp multithreading/openMP.cpp -o openMP && echo "\n" && ./openMP