### usefull url
0. [stl containers](https://stackoverflow.com/questions/471432/in-which-scenario-do-i-use-a-particular-stl-container)
1. https://www.stdio.vn/modern-cpp/std-thread-trong-c-cQQFs
2. [Tổng quát về lập trình đa luồng trong Modern C++](https://codecungnhau.com/category/c/c-multithreading/)
3. viblo-asia [ multithreading C++](https://viblo.asia/p/lam-quen-voi-multithreading-trong-c-qm6RWQYXGeJE) P1.
4. viblo-asia [multithreading C++](https://viblo.asia/p/lam-quen-voi-multithreading-p2-AQ3vVka1RbOr) P2.
5. [concurrency programming C++ 11](https://viblo.asia/p/concurrency-in-c11-LzD5dOLzljY)
6. [openMP](https://viblo.asia/p/gioi-thieu-openmp-trong-c-phan-1-GrLZDAEBlk0#_refs-12)
7. [concurrency programming in swift](https://viblo.asia/p/concurrency-programming-guide-63vKjpYdl2R).

### TODO
1. implement conv2D(in_channels, out_channels, 
                    kernel_size, stride=1, padding=0, 
                    dilation=1, groups=1, 
                    bias=True, padding_mode='zeros', 
                    device=None, dtype=None)
2. đo thời gian thực thi chương trình
3. so sánh thời gian chạy multi-threading và single-threading
4. overload function với các kiểu dữ liệu khác nhau 

### Proplem with multithreading
1. [multithreading vs single-threading](https://codelearn.io/sharing/da-luong-nhanh-hay-cham), what, why, when ?

### library for Parallel processing
1. [Threading Building Blocks](https://github.com/oneapi-src/oneTBB)
2. [openMP](https://en.wikipedia.org/wiki/OpenMP)
3. [High Performance ParalleX](https://github.com/STEllAR-GROUP/hpx)
