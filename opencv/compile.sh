#! /bin/sh

# https://answers.opencv.org/question/46755/first-example-code-error/
# -lopencv_videoio \
# https://www.google.com/search?q=cannot+find+-lopencv_core%3A&sxsrf=ALiCzsa8IZz81GWb3_9Jg_5DDMjgh4w6Rg%3A1663773648154&ei=0CsrY9SGCcremAX33J7YBQ&ved=0ahUKEwjUrqPql6b6AhVKL6YKHXeuB1sQ4dUDCA4&uact=5&oq=cannot+find+-lopencv_core%3A&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQA0oECEEYAEoECEYYAFAAWABgrANoAHAAeACAAVaIAVaSAQExmAEAoAEBwAEB&sclient=gws-wiz-serp
# https://stackoverflow.com/questions/16710047/usr-bin-ld-cannot-find-lnameofthelibrary
# https://stackoverflow.com/questions/15320267/package-opencv-was-not-found-in-the-pkg-config-search-path
# https://stackoverflow.com/questions/49353511/opencv-undefined-reference-to
clear

g++-11 \
-std=c++11 \
-I 3rd_party/opencv-4.6.0/include/opencv4 \
-L 3rd_party/opencv-4.6.0/lib \
-lopencv_core \
-lopencv_highgui \
-lopencv_imgcodecs \
opencv/main.cpp \
-o main 

# g++ -o main opencv/main.cpp -std=c++11 \
# $(pkg-config opencv \
# -I 3rd_party/opencv-4.6.0/include/opencv4 -L 3rd_party/opencv-4.6.0/lib \
# -lopencv_core \
# -lopencv_highgui \
# -lopencv_imgcodecs )

./main opencv/Earth-Horizon.jpg
