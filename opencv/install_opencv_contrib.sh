cd ~
conda deactivate && conda deactivate

# download and install eigen
wget https://gitlab.com/libeigen/eigen/-/archive/3.4.0/eigen-3.4.0.zip
unzip eigen-3.4.0.zip && rm eigen-3.4.0.zip 
sudo cp -rf eigen-3.4.0/ /usr/include
rm eigen-3.4.0/


sudo apt-get upgrade libstdc++6 -y
sudo apt install libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev -y
sudo apt install libgtk-3-dev -y
sudo apt install libpng-dev libjpeg-dev libopenexr-dev libtiff-dev libwebp-dev -y
sudo apt install build-essential pkg-config yasm checkinstall -y
sudo apt install libavcodec-dev libavformat-dev libswscale-dev -y
sudo apt install libxvidcore-dev x264 libx264-dev libfaac-dev libmp3lame-dev libtheora-dev -y
sudo apt install libvorbis-dev -y
sudo apt install libopencore-amrnb-dev libopencore-amrwb-dev -y
sudo apt install libgtk2.0-dev -y

# https://askubuntu.com/questions/342202/failed-to-load-module-canberra-gtk-module-but-already-installed
sudo apt install libcanberra-gtk-module libcanberra-gtk3-module -y
sudo apt-get install libtbb-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install libprotobuf-dev protobuf-compiler -y
sudo apt-get install libgoogle-glog-dev libgflags-dev -y
sudo apt-get install libgphoto2-dev libeigen3-dev libhdf5-dev doxygen -y
sudo apt-get install build-essential libgtk-3-dev -y    
sudo apt-get install gtk2.0 -y
sudo apt-get install libdc1394-dev -y 
sudo apt-get -y install freetype2-demos
sudo apt-get install -y libgtkglext1
sudo apt-get install libgtkglext1-dev -y
sudo apt-get install libopencv-dev -y



# Install minimal prerequisites (Ubuntu 18.04 as reference)
sudo apt update && sudo apt install -y cmake g++ wget unzip
# Download and unpack sources
wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip
wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.x.zip
unzip opencv.zip
unzip opencv_contrib.zip
# Create build directory and switch into it
mkdir -p build && cd build

# Configure
# cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.x/modules ../opencv-4.x

cmake \
    -D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.6.0/modules \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=~/Documents/programming/C++Python/3rd_party/openCV-4.6.0 \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=ON \
    -D WITH_EIGEN=ON \
    -D EIGEN_INCLUDE_PATH=~/Documents/programming/C++Python/3rd_party/eigen-3.4.0 \
    -D WITH_CUDA=ON \
    -D WITH_CUDNN=ON \
    -D OPENCV_DNN_CUDA=OFF \
    -D ENABLE_FAST_MATH=1 \
    -D CUDA_FAST_MATH=1 \
    -D WITH_CUBLAS=ON \
    -D WITH_TBB=ON \
    -D WITH_V4L=ON \
    -D WITH_QT=OFF \
    -D WITH_OPENGL=OFF \
    -D WITH_GSTREAMER=ON \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D OPENCV_ENABLE_NONFREE=ON \
    -D HAVE_opencv_python3=ON \
    -D OPENCV_PYTHON3_INSTALL_PATH=~/anaconda3/envs/py3/lib/python3.9/site-packages \
    -D PYTHON_EXECUTABLE=~/anaconda3/envs/py3/bin/python \
    -D BUILD_EXAMPLES=ON \
    -D BUILD_JAVA=OFF \
    -D BUILD_DOCS=ON  \
    -D CUDA_ARCH_BIN=7.5 \
    -D HIGHGUI_ENABLE_PLUGINS=ON \
    -D HIGHGUI_PLUGIN_LIST=gtk2 \
    ../opencv-4.x 

-D ENABLE_FAST_MATH=1 \
-D CUDA_FAST_MATH=1 \
-D WITH_CUDA=ON \
-D WITH_CUDNN=ON \
-D WITH_CUBLAS=ON \
-D OPENCV_DNN_CUDA=OFF \
-D CUDA_ARCH_BIN=7.5 \
-D HAVE_opencv_python3=ON \
-D OPENCV_PYTHON3_INSTALL_PATH=~/anaconda3/envs/py3/lib/python3.9/site-packages \
-D PYTHON_EXECUTABLE=~/anaconda3/envs/py3/bin/python \
-D HIGHGUI_ENABLE_PLUGINS=ON \
-D HIGHGUI_PLUGIN_LIST=all \

# 4.6.0
cmake \
-D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.6.0/modules \
-D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=~/Documents/programming/C++Python/3rd_party/openCV-4.6.0 \
-D INSTALL_PYTHON_EXAMPLES=ON \
-D INSTALL_C_EXAMPLES=ON \
-D WITH_TBB=ON \
-D WITH_OPENMP=ON\
-D WITH_EIGEN=ON \
-D EIGEN_INCLUDE_PATH=~/Documents/programming/C++Python/3rd_party/eigen-3.4.0 \
-D WITH_OPENGL=OFF \
-D WITH_GTK=ON \
-D HAVE_opencv_python3=ON \
-D OPENCV_PYTHON3_INSTALL_PATH=~/anaconda3/envs/py39/lib/python3.9/site-packages \
-D PYTHON_EXECUTABLE=~/anaconda3/envs/py39/bin/python \
-D BUILD_EXAMPLES=ON \
-D BUILD_JAVA=OFF \
-D BUILD_DOCS=OFF \
-D HIGHGUI_ENABLE_PLUGINS=ON \
-D HIGHGUI_PLUGIN_LIST=gtk,gtk2,gtk3,all \
../opencv-4.6.0

# Build
# cmake --build .
make -j16

# Install
sudo make install


# OFF WITH_OPENGL with your turn ON WITH_GTK because this GKT is looking for GTK-3
# https://forum.opencv.org/t/building-opencv-with-opengl-build-error/7106/12