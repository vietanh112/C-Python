// Standard library search and sort algorithms.
#include <iostream>
using std::cout;
using std::endl;

#include <algorithm>  // algorithm definitions
#include <vector>     // vector class-template definition
#include <iterator>

bool greater10( int value );  // prototype

template < class T >
void printVector( const std::vector< T > &integers2 );

const int SIZE = 10;

int main(){
    int a[ SIZE ] = { 10, 2, 17, 5, 16, 8, 13, 11, 20, 7 };

    std::vector< int > v( a, a + SIZE );
    std::ostream_iterator< int > output( cout, " " );

    cout << "Vector v contains: " << endl;
    std::copy( v.begin(), v.end(), output );
    cout << '\n';

    
    // locate first occurrence of 16 in v          
    std::vector< int >::iterator location;         
    location = std::find( v.begin(), v.end(), 16 );

    if ( location != v.end() ) 
        cout << "Found 16 at location \n" 
            << ( location - v.begin() );
    else 
        cout << "16 not found \n";
    
    // locate first occurrence of 100 in vector v          
    location = std::find( v.begin(), v.end(), 100 );

    if ( location != v.end() ) 
        cout << "Found 100 at location " 
            << ( location - v.begin() ) << '\n';
    else 
        cout << "100 not found" << '\n';

    // locate first occurrence of value greater than 10 in vector v
    location = std::find_if( v.begin(), v.end(), greater10 );

    if ( location != v.end() ) 
        cout << '\n' 
             << "The first value greater than 10 is "
             << *location << " found at location " 
             << ( location - v.begin() ) << '\n';
    else 
        cout << '\n' <<"No values greater than 10 were found" << '\n';

    // sort elements of v
    std::sort( v.begin(), v.end() );
    cout << "\n====================================================\n"
         << "Vector v after sort: \n";
    // printVector(v);
    std::copy( v.begin(), v.end(), output ); // copy sang toàn bộ giá trị của mảng sang mảng mới, đồng thời print 
    cout << '\n';
    
    // use binary_search to locate 13 in v
    if ( std::binary_search( v.begin(), v.end(), 13 ) )
        cout << "13 was     found in vector v" << '\n';
    else
        cout << "13 was not found in vector v" << '\n';

    // use binary_search to locate 100 in vector v
    if ( std::binary_search( v.begin(), v.end(), 100 ) )
        cout << "100 was     found in vector v" << endl;
    else
        cout << "100 was not found in vector v" << endl;
    
    return 0;

}

// determine whether argument is greater than 10
bool greater10( int value ) {
    return value > 10;
}

template < class T >
void printVector( const std::vector< T > &ints2 ){
    typename std::vector < T > :: const_iterator it;

    for (it = ints2.begin(); it != ints2.end(); it ++ )
        cout << *it << ' ';
    cout << '\n';
}