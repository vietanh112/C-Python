// Fig. 21.14: fig21_14.cpp
// Demonstrating standard library vector class template.
#include <iostream>

using std::cout;
using std::cin;
using std::endl;

#include <vector>  // vector class-template definition

// prototype for function template printVector
template < class T >
void printVector( const std::vector< T > &integers2 );

int main(){
    std::vector< int > integers;

    cout << "The initial size of integers is: " 
         << integers.size() << '\n'
         << "The initial capacity of integers is: " 
         << integers.capacity() << '\n'
         << "-------------------------------------" << '\n';

    // function push_back is in every sequence collection
    integers.push_back( 2 );
    integers.push_back( 3 );
    integers.push_back( 4 );
    // integers.push_back( 5 );
    // integers.push_back( 6 );
    cout << '\n' 
         << "The size of     integers is: " << integers.size()     << '\n'
         << "The capacity of integers is: " << integers.capacity() << '\n';

    cout << "Output array  using pointer  notation: ";
    const int SIZE = 6;
    int array[ SIZE ] = { 1, 2, 3, 4, 5, 6 };
    for ( int *ptr = array; ptr != array + SIZE; ++ ptr )
        cout << *ptr << ' ';
    cout << '\n';

    cout << "Output vector using iterator notation: ";
    printVector( integers );


    cout << "Reversed contents of vector  integers: ";
    std::vector< int >::reverse_iterator it;
    for ( it = integers.rbegin(); it != integers.rend(); ++ it)                 
        cout << *it << ' ';
    cout << endl;

    return 0;
}

template < class T >
void printVector( const std::vector< T > &ints2 ){
    // https://stackoverflow.com/questions/20866892/c-iterator-with-template
    typename std::vector < T > :: const_iterator it;

    for (it = ints2.begin(); it != ints2.end(); it ++ )
        cout << *it << ' ';
    cout << '\n';
}
