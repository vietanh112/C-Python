#include <iostream>
#include <stack>
#include <vector>
#include <list>

// popElements function-template prototype 
template< class T >
void popElements( T &stackRef );

int main(){
    // stack with default underlying deque
    std::stack< int > intDequeStack;      

    // stack with underlying vector
    std::stack< int, std::vector< int > > intVectorStack;

    // stack with underlying list
    std::stack< int, std::list< int > > intListStack;

    // push the values 0-9 onto each stack
    for ( int i = 0; i < 10; ++i ) {
        intDequeStack.push( i ); 
        intVectorStack.push( i );
        intListStack.push( i );  
    }

    // display and remove elements from each stack
    std::cout << "Popping from intDequeStack:  ";
    popElements( intDequeStack );
    std::cout << "Popping from intVectorStack: ";
    popElements( intVectorStack );
    std::cout << "Popping from intListStack:   ";
    popElements( intListStack );


    std::cout << std::endl;
    return 0;
}

// pop elements from stack object to which stackRef refers
template< class T >
void popElements( T &stackRef ){
    while ( !stackRef.empty() ) {
        std::cout << stackRef.top() << ' ';  // view top element  
        stackRef.pop();                 // remove top element
    }
    std::cout << std::endl;
}
