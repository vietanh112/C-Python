from ctypes import cdll
lib = cdll.LoadLibrary('./libgeek.so')

# create a Geek class
class Geek(object):
    def __init__(self):
        self.obj = lib.Geek_new()
    # define method
    def myFunction(self):
        lib.Geek_myFunction(self.obj)

# create a Geek class object
f = Geek()
x = lib.add(1000, 2)
print(x)
# object method calling
f.myFunction()