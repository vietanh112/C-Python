#include <iostream>


class Geek{
    public:
        void myFunction(){
            std::cout << "Hello Geek!!!" << std::endl;
        }
};

extern "C" {
    Geek* Geek_new(){ return new Geek(); }
    void Geek_myFunction(Geek* geek){ geek -> myFunction(); }
    int add(int a, int b){
        return a + b;
}
}


// int main() {
//     Geek t;             // Creating an object
//     t.myFunction();     // Calling function

//     return 0;
// }
