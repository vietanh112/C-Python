#include <iostream>


void prefix_vs_suffix(){
    int x = 3;
    int y, z;
    y = ++x; // x = x+1; y = x; ++ tiền tố(prefix) sẽ thực hiện trước

    std::cout << "x = 3; y = ++x; " << "x = " << x << " ; y = "<< y << '\n';
    std::cout << "           ^~~~~~             ++ tiền tố(prefix) sẽ thực hiện trước" << "\n\n";
    
    x = 3;
    z = x++; // (z = x); x = x+1; ++ hậu tố (suffix) sẽ thực hiện sau
    std::cout <<"x = 3; z = x++; " << "x = "<< x << " ; z = " << z << '\n';
    std::cout << "           ^~~~~~             ++ hậu tố (suffix) sẽ thực hiện sau" << "\n\n";

}

int main(){
    prefix_vs_suffix();
    return 0;
}