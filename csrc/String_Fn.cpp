#include "String_Fn.h"

string inHoa(string xau) {
    for (int i = 0; i < xau.size(); i++) {
        if (xau[i] >= 'a' && xau[i] <= 'z')
            xau[i] = xau[i] - 'a' + 'A';
    }
    return xau;
}

string inThuong(string xau) {
    for (int i = 0; i < xau.size(); i++) {
        if (xau[i] >= 'A' && xau[i] <= 'Z')
            xau[i] = xau[i] - 'A' + 'a';
    }
    return xau;
}