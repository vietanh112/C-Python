#include <iostream>
using namespace std;



class Person {
public:
	Person(int x) { cout << "0 - Person::Person(int ) called" << endl; }
    Person()      { cout << "    Person::Person()     called" << endl; }
    
    std::string getName(){
        return std::string("Person class");
    }
};


class Faculty : virtual public Person {
public:
    Faculty(int x):Person(x){
	cout<<"1 - Faculty::Faculty called"<< endl;
	}
    
    std::string getNameFaculty(){
        cout << "Faculty class" << endl;
        return "Faculty class";
    }
};


class Student : virtual public Person {
public:
	Student(int x):Person(x) {
		cout<<"2 - Student::Student called"<< endl;
	}
};


class TA : public Faculty, public Student {
public:
	// TA(int x) : Student(x), Faculty(x) 
    TA(int x) : Student(x), Faculty(x), Person(x)
    {
		cout<<"TA::TA called"<< endl;
	}
};


void solve_the_diamond_problem(){
    /*
    cách giải quyết vấn đề kim cương trong đa kế thừa
    vãn chưa call đc Person::getName()

    */
    // cout << "----------------------------" << endl;
	TA ta1(30);

    cout << "----------------------------" << endl;
    ta1.getNameFaculty();
    ta1.getName();  
}

int main() {
    // https://www.geeksforgeeks.org/multiple-inheritance-in-c/
    // https://codecungnhau.com/diamond-problem-trong-da-thua-ke/
    
    solve_the_diamond_problem();
    return 0;
}
