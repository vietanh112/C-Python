#include <iostream>
#include <bits/stdc++.h>


class Point{
    private:
        float x;
        float y;
        float z;

    public:
        Point(){}

        Point(float x, float y, float z){
            this->x = x;
            this->y = y;
            this->z = z;
        }
        Point(const Point &point){
            this->x = point.x;
            this->y = point.y;
            this->z = point.z;
        }
};
