import os
from distutils.core import setup
from Cython.Build import cythonize


ext_modules = cythonize('prime_number.pyx', 
                        nthreads=os.cpu_count(), 
                        compiler_directives={'language_level' : '3'})

setup(ext_modules=ext_modules)

