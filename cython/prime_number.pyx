# distutils: language = c++
from libcpp cimport bool
from math import ceil, sqrt, floor

cpdef bool is_prime(int n):
    if n < 2:       return False
    elif not n % 2: return n == 2

    max_range = floor(sqrt(n) + 1)
    for i in range(3, max_range, 2):
        if not n % i: 
            return False
    return True
