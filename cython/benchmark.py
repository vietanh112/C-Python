from fastfac import fastfactorial
from fac import factorial
from timeit import timeit


print(timeit('fastfactorial(100)', globals=globals(), number=10000))
print(timeit('factorial(100)', globals=globals(), number=10000))