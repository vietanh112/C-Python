### setup

- [ ] cài đặt gcc/g++ 
- [ ] cài đặt GDB `sudo apt-get install build-essential gdb`
- [ ] tạo folder `.vscode`
- [ ] tạo `.vscode/tasks.json` setup default compiler và các flag để **build** [link](https://code.visualstudio.com/docs/cpp/config-linux#_run-helloworldcpp)
    + "${fileDirname}/bin/${fileBasenameNoExtension}" thay đổi vị trí lưu file sau compile. [link](https://www.youtube.com/watch?v=9pjBseGfEPU)
- [ ] tạo `.vscode/launch.json` để ấn F5 là có thể bắt đầu **debug** [Link](https://devblogs.microsoft.com/cppblog/visual-studio-code-c-c-extension-july-2019-update/)
- [ ] cài đặt C/C++ configurations `c_cpp_properties.json` [link](https://code.visualstudio.com/docs/cpp/config-linux#_cc-configurations)
- [ ] chỉnh include path trong `.vscode/c_cpp_properties.json`