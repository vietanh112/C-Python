#include <iostream>

// using namespace std;


float taxi_fee(float distance){
    float fee = 0.0;

    // Số km ≤ 1 giá 15000đ
    if (distance <= 1){
        fee = 15000.0;
    }
    
    // 1 < số km ≤ 5 giá 13500đ
    else if ( 1 < distance <= 5){
        fee = 15000.0 + 13500 * (distance - 1);
    }

    // Số km > 5 giá 11000đ
    else if (distance > 5){
        fee = 15000.0 + 5*13500 + (distance-5)*11000;
    }

    // Nếu số km > 120 km sẽ được giảm 10% trên tổng số tiền.
    if (distance > 120){
        fee *= 0.9;
    }
    return fee;
}

int main(){
    /**
    Viết hàm tính tiền đi taxi từ số km cho trước, biết:

    Số km ≤ 1 giá 15000đ
    1 < số km ≤ 5 giá 13500đ
    Số km > 5 giá 11000đ
    Nếu số km > 120 km sẽ được giảm 10% trên tổng số tiền.
    */
    float distance = 1.5;
    float sotien = taxi_fee(distance);
    std::cout << sotien << '\n';
    return 0;
}