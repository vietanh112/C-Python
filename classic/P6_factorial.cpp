#include <iostream>


int factorial(int n){
    int fac = 1;
    for (int i = 2; i <= n; i++){
        fac *= i;
    }
    return fac;
}

int main(){
    int num = 5;
    int fac = factorial(num);
    std::cout << fac << '\n';
    return 0;
}