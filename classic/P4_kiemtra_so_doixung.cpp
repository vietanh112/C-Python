#include <iostream>
#include <bits/stdc++.h>


int reverse(int num){
    int n = std::floor(std::log10(num) + 1);
    // std::cout << "so luong cac chu so n = " << n << std::endl;

    int digits[n];
    for (int i = n - 1 ; i >= 0; i-- ){
        digits[i] = num % 10;
        num /= 10;
    }

    int rev = 0;
    for (int i = 0; i < n; i++){
        rev += pow(10, i) * digits[i];
        // std::cout <<  digits[i] << " --> "<< rev << std::endl;
    }
    return rev;
}


bool is_symmetric(int num){
    int num_reverse = reverse(num);
    if (num == num_reverse){
        return true;
    }
    else 
        return false;
}

int main(){
    /*
    1. đảo ngược lại số
    2. so sánh số đảo ngược với số ban đầu
    3. nếu bằng nhau thì là Symmetric; ngược lại là Asymmetric
    */
    int num = 121;
    if (is_symmetric(num)){
        std::cout << num <<" Symmetric" << '\n';
    }
    else
        std::cout << num << " Asymmetric" << '\n';

    return 0;
}