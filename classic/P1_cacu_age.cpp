#include <iostream>
#include <ctime>

// https://howkteam.vn/course/bai-toan-kinh-dien-trong-lap-trinh/tinh-so-tuoi-nguoi-dung-1276

int get_yyyy_now(){
    //https://www.tutorialspoint.com/cplusplus/cpp_date_time.htm
    time_t now = time(0);
    tm* ltm = localtime(&now);
    int yyyy = 1900 + ltm->tm_year;
    // cout << yyyy << endl;
    return yyyy;
}

int main(){
    int yyyy = get_yyyy_now();
    int yob = 1991;

    int age = yyyy - yob;
    std::cout << age << std::endl;
    return 0;
}