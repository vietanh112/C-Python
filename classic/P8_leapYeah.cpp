#include <iostream>


bool is_leap_yeah(int yyyy){
    /**
     * chia hết cho 4 và k chia hết cho 100
     * nếu chia hết cho 100 thì cũng phải chia hết cho 400
     */

    if (yyyy % 400 == 0) return true;

    if ((yyyy % 4 == 0) && (yyyy %100 != 0)) 
        return true;
    else 
        return false;
}

int main(){
    int yyyy = 2001;
    std::cout << is_leap_yeah(yyyy) << std::endl;
    return 0;
}