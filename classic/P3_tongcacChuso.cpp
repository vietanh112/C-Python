#include <iostream>

int tong(int num){
    int _sum = 0;
    do{
        _sum += num%10;
        num /= 10;
    } 
    while (num > 0);

    return _sum;
}


int main(){
    int songuyen = 654321;
    int tong_cac_chu_so = tong(songuyen);
    std::cout << tong_cac_chu_so << '\n';

    return 0;
}